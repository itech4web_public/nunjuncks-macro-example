## Installation

1. `npm install -g @11ty/eleventy`

2. `cd /path/to/project`

3. ``` eleventy ``` - will generate static files in `_site` directory with `index.html`

### or simply run:

`npx @11ty/eleventy`
